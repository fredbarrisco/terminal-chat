import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class TcpChatServer {

    public final static int DEFAULT_PORT = 6666;
    public final String LIST_CMD = "/LIST";
    private List<ServerWorker> workers = Collections.synchronizedList(new ArrayList<ServerWorker>());


    public static void main(String[] args) {

        int port = DEFAULT_PORT;

        try {
            if (args.length > 0) {
                port = Integer.parseInt(args[0]);
            }
            TcpChatServer server = new TcpChatServer();
            server.start(port);

        } catch (NumberFormatException ex) {
            System.out.println("Usage: java ChatServer [port_number]");
            System.exit(1);
        }
    }


    public void start(int port) {

        System.out.println("DEBUG: Server instance is : " + this);

        int connectionCount = 0;

        try {
            // Bind to local port
            System.out.println("Binding to port " + port + ", please wait  ...");
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server started: " + serverSocket);

            while (true) {
                // Block waiting for client connections
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client accepted: " + clientSocket);

                try {
                    // Create a new Server Worker
                    connectionCount++;
                    String name = "Client-" + connectionCount;
                    ServerWorker worker = new ServerWorker(name, clientSocket);
                    workers.add(worker);

                    // Serve the client connection with a new Thread
                    Thread thread = new Thread(worker);
                    thread.setName(name);
                    thread.start();

                } catch (IOException ex) {
                    System.out.println("Error receiving client connection: " + ex.getMessage());
                }
            }
        } catch (IOException e) {
            System.out.println("Unable to start server on port " + port);
        }
    }

    public String listClients() {

        StringBuilder builder = new StringBuilder("\n\n");

        synchronized (workers) {
            Iterator<ServerWorker> it = workers.iterator();
            while (it.hasNext()) {
                builder.append("\t");
                builder.append(it.next().getName());
                builder.append("\n");
            }
        }
        return builder.toString();
    }

    private void sendAll(String origClient, String message) {
        // Acquire lock for safe iteration
        synchronized (workers) {

            Iterator<ServerWorker> it = workers.iterator();
            while (it.hasNext()) {
                it.next().send(origClient, message);
            }
        }
    }

    private class ServerWorker implements Runnable {

        // Immutable state, no need to lock
        final private String name;
        final private Socket clientSocket;
        final private BufferedReader in;
        final private BufferedWriter out;

        private ServerWorker(String name, Socket clientSocket) throws IOException {

            this.name = name;
            this.clientSocket = clientSocket;

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
        }

        public String getName() {
            return name;
        }

        @Override
        public void run() {

            System.out.println("Thread " + name + " started");

            try {
                while (!clientSocket.isClosed()) {
                    // Blocks waiting for client messages
                    String line = in.readLine();

                    if (line == null) {
                        System.out.println("Client " + name + " closed, exiting...");
                        in.close();
                        clientSocket.close();
                        continue;

                    } else if (!line.isEmpty()) {
                        if (line.toUpperCase().equals(LIST_CMD)) {
                            send("Clients Connected", listClients());
                        } else {
                            // Broadcast message to all other clients
                            sendAll(name, line);
                        }
                    }
                }
                workers.remove(this);
            } catch (IOException ex) {
                System.out.println("Receiving error on " + name + " : " + ex.getMessage());
            }
        }

        private void send(String origClient, String message) {
            try {
                out.write(origClient + ": " + message);
                out.newLine();
                out.flush();
            } catch (IOException ex) {
                System.out.println("Error sending message to Client " + name + " : " + ex.getMessage());
            }
        }
    }
}

